# Interview Exercise

Your goal is to create an iOS or Android app using React Native with a screen that allows a user to pick a set of custom numbers for a lottery ticket.

Powerball is a popular US lottery game with draws twice a week. For the purposes of this exercise, a Powerball lottery "ticket" is a set of 5 integers (from `1`-`69`) along with a 6th integer (the _Powerball_, from `1`-`26`) that the user has chosen to play during a specific draw.

For example:

`02 14 19 21 61` `25`


# Designs

- The following designs can be used to guide you. If you want to alter them or add improvements, feel free to let your creative juices flow!<br/>

![](https://i.imgsafe.org/e8/e8883491ad.png) ![](https://i.imgsafe.org/e8/e88f0dbbde.png)
![](https://i.imgsafe.org/e8/e88f0dc3bf.png) ![](https://i.imgsafe.org/e8/e88f1da195.png)

## Exercise Rules

- There is a 3 hour time limit for this challenge. 
- Use your best discretion with the design and requirements, but you can ask questions.
- Follow modern JavaScript and React Native best practices and conventions to the best of your ability.
- You are free to add packages, tools or improvements to your project as you see fit.
- Please submit your code via a GitHub repository.


## Tips

- You only need to worry about optimizing for one platform, either iOS or Android, depending on your preference.
- Although not necessary, animations are a big bonus! Show us your skills!

## Contact

We encourage you to use your best discretion, but also to ask questions and communicate if you need it.

If you have questions during the interview, call me contact or send a message to:

- [vikas@digitalagents.in](mailto:vikas@digitalagents.in)
